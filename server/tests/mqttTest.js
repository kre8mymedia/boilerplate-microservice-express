const Mqtt = require('./services/Mqtt.js')

const mqtt = new Mqtt();

const mqtt = require('mqtt')
const mqttClient = mqtt.connect({
    host: process.env.MQTT_HOST,
    port: process.env.MQTT_PORT,
    username: process.env.MQTT_USERNAME,
    password: process.env.MQTT_PASSWORD,
    rejectUnauthorized: false
});

const topic = 'cpu_temp/#'
console.log('Topic: ', topic)

mqttClient.subscribe(topic)

mqttClient.on('message', function (topic, message) {
    let topic_str = topic.toString()
    let message_str = message.toString()
    // split the topic str into array
    topic_str = topic_str.split('/')
    console.log(message_str)
})