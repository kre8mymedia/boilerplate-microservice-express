require('dotenv').config()
const mqtt = require('mqtt')

class Mqtt {
    constructor(){
        this.clientId = `mqtt_${Math.random().toString(16).slice(3)}`;
        this.client = mqtt.connect({
            host: process.env.MQTT_HOST,
            port: process.env.MQTT_PORT,
            username: process.env.MQTT_USERNAME,
            password: process.env.MQTT_PASSWORD,
            rejectUnauthorized: false
        })
    }

    async publish(topic, message) {
        try {
            this.client.publish(topic, message, { qos: 0, retain: false }, (error) => {
                if (error) {
                    console.error(error)
                }
            })
            console.log(`Published: ${topic}`)
        } catch (e) {
            throw new Error(e.message())
        }
    }
}

module.exports = Mqtt