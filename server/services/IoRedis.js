require('dotenv').config()
const Redis = require("ioredis");

const redis = new Redis('redis', 6379);

class IoRedisService {

    async getAll() {
        const keys = await redis.keys('*')
        return keys
    }

    async getByKey(key) {
        const val = await redis.get(key)
        return val
    }

}

module.exports = IoRedisService