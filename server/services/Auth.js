require('dotenv').config()
// const axios = require('axios')

const jwt = require('jsonwebtoken')

class Auth {

    async hello() {
        try {
            return `Hello from auth service`
        } catch (e) {
            throw new Error(e)
        }
    }

    async authenticateToken(req, res, nxt) {
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(" ")[1]
        if (token == null) return res.sendStatus(401)
    
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if (err) return res.sendStatus(403)
            req.user = user
            nxt()
        })
    }
}

module.exports = Auth