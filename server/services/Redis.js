require('dotenv').config()
const redis = require("redis")

class RedisService {

    static get Url() {
        return Url;
    }

    async set(key, value) {

        const redisClient = redis.createClient({
            url: 'redis://redis:6379'
        });
        redisClient.on('error', (err) => console.log('Redis Client Error', err));

        await redisClient.connect();
        
        await redisClient.set(key, value);
        const result = await redisClient.get(key);
        // console.log(result)

        return result;
    }

    async get(key) {
        const redisClient = redis.createClient({
            url: 'redis://redis:6379'
        });
        redisClient.on('error', (err) => console.log('Redis Client Error', err));

        await redisClient.connect();
        
        const result = await redisClient.get(key);
        // console.log(result)
        return result;
    }

    async getAll() {

        const redisClient = redis.createClient({
            url: 'redis://redis:6379'
        });
        redisClient.on('error', (err) => console.log('Redis Client Error', err));

        await redisClient.connect();
        
        redisClient.keys("*", function(err, replies) {
            if (err) {
              return callback(err);
            }
            client.mget(replies, function(err, replies) {
              if (err) {
                return callback(err);
              }
              return callback(null, replies);
            });
          });
    }

}

module.exports = RedisService