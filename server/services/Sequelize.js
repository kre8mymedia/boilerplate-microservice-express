require('dotenv').config()
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('mysql://express:test1234@db/express')

// Check Database Connection
try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}

class SequelizeService {

    static client() {
        return sequelize;
    }
}

module.exports = SequelizeService