require('dotenv').config()
const axios = require('axios')

class Slack {

    async publish(msg) {
        try {
            const req = await axios.post('https://hooks.slack.com/services/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', {
                text: msg
            })
            console.log(`Slack message published`)
        } catch (e) {
            throw new Error(e)
        }
    }
}

module.exports = Slack