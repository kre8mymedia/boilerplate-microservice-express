require('dotenv').config()
const { create } = require('ipfs-http-client');

const RedisService = require('./Redis');

class Ipfs {
    constructor(){
        this.client = create('http://ipfs:5001')
        this.redisService = new RedisService()
    }

    async add(id, payload) {

        try {
            const file = await this.client.add({
                path: "hello.json",
                content: JSON.stringify(payload),
              });
              
            const key = file.cid.toString()
        
            await this.redisService.set(id, key);
            const result = await this.redisService.get(id);
        
            const obj = {};
            obj[id] = result;
            return obj
        } catch (e) {
            console.log(e)
        }
    }

    async fetch(id) {

        try {
            const result = await this.redisService.get(id);
            const stream = this.client.cat(result)
            let data = ''
            for await (const chunk of stream) {
                // chunks of data are returned as a Buffer, convert it back to a string
                data += chunk.toString()
            }
            return JSON.parse(data)
        } catch (e) {
            console.log(e)
        }
    }
}

module.exports = Ipfs