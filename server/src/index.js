require('dotenv').config()
const express = require('express')

const bodyParser = require("body-parser");
const moment = require('moment');
const winston = require('winston');
const expressWinston = require('express-winston');
const myConsoleFormat = winston.format.printf(function (info) {
  return `${info.level}: ${info.message} (${moment().format('YYYY-MM-DDTHH:mm:ss.SSSZZ')})`;
});

// app.js
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const app = express()

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Event-driven MQTT service based Architecture',
    version: '1.0.0',
    description:
      'This is a boilerplate REST API for creating Event-driven MQTT service based applications',
    license: {
      name: 'Licensed Under MIT',
      url: 'https://spdx.org/licenses/MIT.html',
    },
    contact: {
      name: 'Joe Moma',
      url: 'https://jsonplaceholder.typicode.com',
    },
  },
  servers: [
    {
      url: 'http://localhost:5000',
      description: 'Development server',
    },
  ],
};

const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: [
    './routes/users/*.js',
    './routes/slack/*.js',
    './routes/ipfs/*.js',
  ],
};

const swaggerSpec = swaggerJSDoc(options);

const HOST = process.env.APP_URL
const PORT = process.env.APP_PORT

/** ------------------------------------------------------------------
 * Plugins
 * -------------------------------------------------------------------
 */
app.disable('etag')
app.use(bodyParser.json())
// LOGGER
app.use(expressWinston.logger({
    transports: [
      	new winston.transports.Console()
    ],
    format: winston.format.combine(
		winston.format.colorize(),
		myConsoleFormat
    ),
    meta: false, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
    expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
    colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
    ignoreRoute: function (req, res) { return false; } // optional: allows to skip some log messages based on request and/or response
}))

/** ------------------------------------------------------------------
 * Modules (NEEDS TO GO BELOW THE LOGGER ABOVE)
 * -------------------------------------------------------------------
 */
const api = require("../routes");

/** ------------------------------------------------------------------
 * Routes
 * -------------------------------------------------------------------
 */
app.use(api);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

/** ------------------------------------------------------------------
 * HTTP Routes
 * -------------------------------------------------------------------
 */
app.get("/", async (req, res) => {
    res.status(201).send('<h3>Welcome to home page</h3><br>✨👋🌎🌍🌏✨<br><a href="/about">About</a><br><a href="/contact">Contact</a><br><a href="/docs">Docs</a>')
});
/**
 * @swagger
 * /users:
 *   get:
 *     summary: Retrieve a list of JSONPlaceholder users
 *     description: Retrieve a list of users from JSONPlaceholder. Can be used to populate a list of fake users when prototyping or testing an API.
*/
app.get("/about", async (req, res) => {
  	res.status(201).json({ msg: "Built to tack on cool programs as services" });
});

app.get('/contact', async (req, res) => {
  	res.status(201).send('<h3>ryan.adaptivebiz@gmail.com</h3>')
})

// Run App 
app.listen(PORT, () => {
  	console.log(`>> App listening at ${HOST}:${PORT}`)
})