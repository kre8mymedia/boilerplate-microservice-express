const express = require("express");

// Redis Singleton Connection
const MqttService = require('../../services/Mqtt.js');

const router = express.Router();

/** ------------------------------------------------------------------
 * Redis Routes
 * 
 * -------------------------------------------------------------------
 */
router.get("/mqtt/pub", async (req, res) => {
	const mqtt = new MqttService();
	const msg = JSON.stringify({ msg: "Published to MQTT" })
	await mqtt.publish('write/1245', msg)
	res.status(201).json(msg);
});

router.post("/mqtt/event", async (req, res) => {
	const mqtt = new MqttService();
	await mqtt.publish(`write/${req.body.topic}/test`, JSON.stringify(req.body.message))
	res.status(201).json(req.body);
});

module.exports = router;