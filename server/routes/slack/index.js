const express = require("express");

// Redis Singleton Connection
const SlackService = require('../../services/Slack.js');

const router = express.Router();

/** ------------------------------------------------------------------
 * Slack Routes
 * 
 * -------------------------------------------------------------------
 */
router.get("/slack/pub", async (req, res) => {
	const slack = new SlackService();
	const msg = "This is from http://localhost:5000/slack/pub"
	await slack.publish(msg)
	res.status(200).json({ msg: "Published to SlackService Alerts" }).end();
});

router.get("/test", (req, res) => {
	res.status(200).json({ msg: "testing" }).end();
});

module.exports = router;