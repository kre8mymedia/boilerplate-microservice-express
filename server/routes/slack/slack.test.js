const app = require('./index.js')
const supertest = require("supertest");
const request = supertest(app)

// This passes because 1 === 1
it('Testing to see if Jest works', () => {
    expect(1).toBe(1)
})

describe("GET /test", () => {
    afterEach(() => { 
        jest.clearAllMocks(); 
        jest.resetAllMocks();
      });
      
    describe("When user visits route...", () => {
      test("should respond with a 200 status code", async () => {
        const response = await request(app).get("/test")
        expect(response.statusCode).toBe(200)
        done();
      }, 2000)
    })
  
})