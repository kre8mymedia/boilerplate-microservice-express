const bcrypt = require("bcrypt")
const express = require("express")
const jwt = require('jsonwebtoken')
const { Sequelize } = require('sequelize');

// Database Singleton Connection
const SequelizeService = require('../../services/Sequelize.js');
const sequelize = SequelizeService.client()

// Models
const UserModel = require('../../models/user.js');
const User = UserModel(sequelize, Sequelize)

const router = express.Router();

/** ------------------------------------------------------------------
 * Slack Routes
 * 
 * -------------------------------------------------------------------
 */
router.post('/login', async (req, res) => {
    const user = {
        email: req.body.email,
        password: req.body.password
    }
    const authUser = await checkUser(user.email, user.password)
    if (authUser == false) {
        res.sendStatus(401)
    } 

    const accessToken = generateAccessToken(user)
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
    refreshTokens.push(refreshToken)
    res.json({ accessToken, refreshToken })
});

let refreshTokens = [] // TDOD: Needs to be inserted in database and assigned to user

router.post('/token', (req, res) => {
    const refreshToken = req.body.token
    if (refreshToken == null) return res.sendStatus(401)
    if (!refreshTokens.includes(refreshToken)) return res.sendStatus(403)
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403)
        const accessToken = generateAccessToken({ name: user.name })
        res.json({ accessToken })
    })
});

/** ------------------------------------------------------------------
 * Authenticated Route
 * -------------------------------------------------------------------
 */
router.get('/secret', authenticateToken, (req, res) => {
    res.json({ success: "You've been logged in!!!" })
});

async function checkUser(email, password) {
    const user = await User.findOne({ where: { email } });
    const match = await bcrypt.compare(password, user.password);

    if(match) {
        return true
    } else {
		return false
	}
}


/** ------------------------------------------------------------------
 * Helper functions
 * -------------------------------------------------------------------
 */
function authenticateToken(req, res, nxt) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(" ")[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403)
        req.user = user
        nxt()
    })
}

function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' })
}

module.exports = router;