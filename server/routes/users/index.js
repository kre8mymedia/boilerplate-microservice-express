const express = require("express");
const { Sequelize } = require('sequelize');

// Database Singleton Connection
const SequelizeService = require('../../services/Sequelize.js');
const sequelize = SequelizeService.client()

// Models
const UserModel = require('../../models/user.js');
const User = UserModel(sequelize, Sequelize)

const router = express.Router();

/** ------------------------------------------------------------------
 * User CRUD Routes
 * 
 * -------------------------------------------------------------------
 */
router.get("/users", async (req, res) => {
	const users = await User.findAll({
		attributes: ['id', 'firstName', 'email']
	})

    res.status(200).send({
		count: users.length,
		users
	})
});

// Create User
router.post("/users", async (req, res) => {
	const user = await User.create(req.body, {
		attributes: ['id', 'firstName', 'email']
	})

    res.status(201).send({
		successs: "User Created",
		user: user
	})
});

// Show User
router.get("/users/:id", async (req, res) => {
	const user = await User.findByPk(req.params.id, {
		attributes: ['firstName', 'lastName', 'email']
	});
	if (user === null) {
		console.log('Not found!');
		res.status(404).send({
			error: "Not Found!"
		})
	} else {
		console.log(user instanceof User); // true
		res.status(200).send({
			success: "Fetched user",
			user: user
		}) 
	}
});

// Update User
router.put("/users/:id", async (req, res) => {
	// const user = users.filter(user => user.id == req.params.id)

	const user =await User.update(req.body, {
		attributes: ['firstName', 'lastName', 'email'],
		where: {
		  id: req.params.id
		}
	});

    res.status(201).send({
		success: "Updated user",
		user: user
	})
});

// Delete User
router.delete('/users/:id', async (req, res) => {
	const user = User.destroy({
		attributes: ['firstName', 'lastName', 'email'],
		where: {
			id: req.params.id
		}
	})

    res.status(200).send({
		success: "Deleted user",
		user
	})
})

module.exports = router;