
/**
 * --------------------------------------------------------------------------------------
 * User Schema
 * --------------------------------------------------------------------------------------
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         id:
 *           type: uuid
 *           description: The user ID.
 *           example: 02092a15-dbcb-40b5-957c-ebd063bbb6e3
 *         firstName:
 *           type: string
 *           description: The user's name.
 *           example: Leanne
 *         lastName:
 *           type: string
 *           description: User last name
 *           example: Graham
 *         userName:
 *           type: string
 *           description: The username.
 *           example: Gamer1234
 *         email:
 *           type: string
 *           description: User email
 *           example: 'john@testcom'
 */

/**
 * --------------------------------------------------------------------------------------
 * List users
 * --------------------------------------------------------------------------------------
 * @swagger
 * /users:
 *   get:
 *     tags:
 *       - Users
 *     summary: List Users
 *     description: List all users that exist
 *     responses:
 *       200:
 *         description: A list of users.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  users:
 *                      items:
 *                        $ref: '#/components/schemas/User'
 */


/**
 * --------------------------------------------------------------------------------------
 * Create User
 * --------------------------------------------------------------------------------------
 * 
 * @swagger
 * /users:
 *   post:
 *     tags:
 *       - Users
 *     summary: Create User
 *     description: creates a new user
 *     responses:
 *       201:
 *         description: Creates user
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/User'
*/

 /**
 * --------------------------------------------------------------------------------------
 * Fetch User by ID
 * --------------------------------------------------------------------------------------
 * @swagger
 * /users/{id}:
 *   get:
 *     tags:
 *       - Users
 *     summary: Retreive User by ID
 *     description: retrieve user by id
 *     responses:
 *       200:
 *         description: A single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  user:
 *                      type: object
 *                      $ref: '#/components/schemas/User'
*/

/**
 * --------------------------------------------------------------------------------------
 * Update User by ID
 * --------------------------------------------------------------------------------------
 * @swagger
 * /users/{id}:
 *   put:
 *     tags:
 *       - Users
 *     summary: Update User by ID
 *     description: retrieve user by id
 *     responses:
 *       201:
 *         description: Update single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  user:
 *                      type: object
 *                      $ref: '#/components/schemas/User'
*/

/**
 * --------------------------------------------------------------------------------------
 * Delete User by ID
 * --------------------------------------------------------------------------------------
 * @swagger
 * /users/{id}:
 *   delete:
 *     tags:
 *       - Users
 *     summary: Delete User by ID
 *     description: Delete user by id
 *     responses:
 *       201:
 *         description: Delete single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  user:
 *                      type: object
 *                      $ref: '#/components/schemas/User'
*/
