const express = require("express");
const userApi = require("./users/index.js");
const redisApi = require("./redis/index.js");
const slackApi = require("./slack/index.js");
const mqttApi = require("./mqtt/index.js");
const authApi = require("./auth/index.js");
const ipfsApi = require("./ipfs/index.js");

const router = express.Router();

router.use(userApi);
router.use(redisApi);
router.use(slackApi);
router.use(mqttApi);
router.use(authApi);
router.use(ipfsApi);

module.exports = router;