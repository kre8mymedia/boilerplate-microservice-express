const express = require("express");

// Redis Singleton Connection
const IpfsService = require('../../services/Ipfs.js');

const router = express.Router();

/** ------------------------------------------------------------------
 * IPFS Routes
 * 
 * -------------------------------------------------------------------
 */
router.get('/ipfs/:id', async (req, res) => {
    const ipfs = new IpfsService()
    try {
      const myVal = await ipfs.fetch(req.params.id)
      console.log(myVal)
      res.json({
        ipfs: myVal
      })
    } catch(e) {
      res.status(500).json({ error: e })
    }
  })
  
router.post("/ipfs/:id", async (req, res) => {
    const ipfs = new IpfsService()
    try {
    const myVal = await ipfs.add(req.params.id, req.body)
    console.log(myVal)
    res.json({
      ipfs: myVal
    })
    } catch(e) {
    res.status(500).json({ error: e })
    }
});

module.exports = router;