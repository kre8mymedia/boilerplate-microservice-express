/**
 * --------------------------------------------------------------------------------------
 * Resource Schema
 * --------------------------------------------------------------------------------------
 * @swagger
 * components:
 *   schemas:
 *     Ipfs:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: Id of the resource trying to retrieve.
 *           example: myRedisKeyForIpfsObj
 */


/**
 * --------------------------------------------------------------------------------------
 * Create Resource
 * --------------------------------------------------------------------------------------
 * 
 * @swagger
 * /ipfs/{id}:
 *   post:
 *     tags:
 *       - Ipfs
 *     summary: Create Resource
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     description: creates a new resource
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: ID used to create data object
 *     requestBody:
 *         content:
 *            application/x-www-form-urlencoded:
 *               schema:
 *                  type: object
 *                  properties:
 *                     field1:          # <!--- form field name
 *                        type: string
 *                     field2:          # <!--- form field name
 *                        type: string'
 *     responses:
 *        200:
 *          description: OK
 * 
 * 
 * 
 * definitions:
 *   IpfsRequest:
 *     type: object
 *     required:
 *       - userName
 *     properties:
 *       userName:
 *         type: string
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 */ 
 
/**
 * --------------------------------------------------------------------------------------
 * Fetch Resource by ID
 * --------------------------------------------------------------------------------------
 * @swagger
 * /ipfs/{id}:
 *   get:
 *     tags:
 *       - Ipfs
 *     summary: Retreive Resource by ID
 *     description: retrieve resource by id
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: ID set by user during object creation
 *     responses:
 *       200:
 *         description: A single IPFS resource.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  ipfs:
 *                      type: object
 *                      $ref: "#/components/schemas/Ipfs"
 */

 
