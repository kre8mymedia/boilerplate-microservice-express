const express = require("express");

// Redis Singleton Connection
const RedisService = require('../../services/Redis.js');
const IoRedisService = require('../../services/IoRedis.js');

const router = express.Router();

/** ------------------------------------------------------------------
 * Redis Routes
 * 
 * -------------------------------------------------------------------
 */
// router.get("/redis", async (req, res) => {
// 	const redis = new IoRedisService();
// 	const keys = await redis.getAll()
// 	let result = '';
// 	for (let key in keys) {
// 		const newVal = await redis.getByKey(keys[key])
// 		const valType = typeof newVal
// 		// if (valType == 'number') {
// 		// 	result += `${keys[key]} ${newVal}<br>`;
// 		// }
// 		result += `${keys[key]} ${newVal}<br>`;
// 	}
// 	res.status(200).send(result);
// });

router.get("/redis", async (req, res) => {
	const redis = new IoRedisService();
	const keys = [
		// 'read:cpu_temp:eb7cb373-af76-49f0-87bb-c614a21c46f9',
		'read:mem_info:eb7cb373_af76_49f0_87bb_c614a21c46f9:MemAvailable',
		'read:mem_info:eb7cb373_af76_49f0_87bb_c614a21c46f9:MemFree',
		'read:solana:balances:2FWB1LvNkzo5hWkp1hm3B67UqTSL7vCENRzawcZdqW5G',
		'read:solana:balances:2HyzZfBDffxKCF8biMrVdVHipPjMr3xf53av3W6qPkr9'
	]
	
	let result = '';
	for (let key in keys) {
		const newVal = await redis.getByKey(keys[key])
		// if (valType == 'number') {
		// 	result += `${keys[key]} ${newVal}<br>`;
		// }
		result += `${keys[key]} ${newVal}\n`;
	}
	res.status(200).send(result);
});

router.get("/redis/set/:key", async (req, res) => {
	const redis = new RedisService();
	const result = await redis.set(req.params.key, "32g3f32tg322g")
	res.status(201).json({ msg: result });
});

router.get("/redis/get/:key", async (req, res) => {
	const redis = new RedisService();
	const myVal = await redis.get(req.params.key)
	res.status(201).json({ msg: myVal });
});

module.exports = router;