'use strict';
const bcrypt = require("bcrypt");

function hashPass(pass) {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(pass, salt);
  return hash;
}

const users = [
  {
    id: '6f3f10cd-3024-4b0c-b308-a492f5f5beb7',
    firstName: 'Ryan',
    lastName: 'Eggz',
    userName: 'ryan_adaptive',
    email: 'ryan.adaptivebiz@gmail.com',
    password: hashPass('test1234'),
    createdAt: '2021-12-21 00:58:11',
    updatedAt: '2021-12-21 00:58:11'
    // isBetaMember: false
  },
  {
    id: 'b279de35-3f49-4e2f-b24a-0c05f2c77e5d',
    firstName: 'John',
    lastName: 'Test',
    userName: 'johnnyT',
    email: 'john@test.com',
    password: hashPass('test1234'),
    createdAt: '2021-12-21 00:58:11',
    updatedAt: '2021-12-21 00:58:11'
    // isBetaMember: false
  },
  {
    id: '6b433a27-07c5-461d-8b97-bbe775644b2b',
    firstName: 'Henry',
    lastName: 'Test',
    email: 'henry@test.com',
    userName: 'henryT',
    password: hashPass('test1234'),
    createdAt: '2021-12-21 00:58:11',
    updatedAt: '2021-12-21 00:58:11'
    // isBetaMember: false
  },
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Users', users, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Users', users, {});
  }
};
