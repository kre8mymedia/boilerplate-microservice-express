require('dotenv').config()
const mqtt = require('mqtt')
const redis = require('redis');

/**------------------------------------------------------------------------------------
 * Get subscriber credentials
 * ------------------------------------------------------------------------------------
 */
const redisObj = {
  // url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
  url: process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD,
}
const redisClient = redis.createClient(redisObj);
const mqttClient = mqtt.connect({
  host: process.env.MQTT_HOST,
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: false
});

/**------------------------------------------------------------------------------------
 * Subscription Channel this listener is on.
 * ------------------------------------------------------------------------------------
 */
const topic = process.env.TOPIC
mqttClient.subscribe(topic)
console.log('Topic: ', topic)

/**------------------------------------------------------------------------------------
 * Takes in a topic and message in order to format for Redis
 * ------------------------------------------------------------------------------------
 */
async function saveRedis(topic, message) {
  let topic_str = topic.toString()
  // let message_str = JSON.parse(message.toString())
  let message_str = message.toString()
  // split the topic str into array
  topic_str = topic_str
                    .replace(/[/]/g, ':')
  console.log(message_str)
  redisClient.setex(`${topic_str}`, 3000, message_str, redis.print);
}

/**------------------------------------------------------------------------------------
 * Will trigger event and save the response in redis. Replaces the slashes with colons.
 * ------------------------------------------------------------------------------------
 */
mqttClient.on('message', async function (topic, message) {
    await saveRedis(topic, message)
})
  