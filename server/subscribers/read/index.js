require('dotenv').config()
const mqtt = require('mqtt')
const redis = require('redis');

/**------------------------------------------------------------------------------------
 * Get subscriber credentials
 * ------------------------------------------------------------------------------------
 */
const redisObj = {
  url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
}
const redisClient = redis.createClient(redisObj);


/**------------------------------------------------------------------------------------
 * Takes in a topic and message in order to format for Redis
 * ------------------------------------------------------------------------------------
 */
async function saveRedis(topic, message) {
  
  let topic_str = topic.toString()
  console.log(topic_str)

  let message_str = message.toString()
  topic_str = topic_str
                    .replace(/[/]/g, ':')
                    .replace(/[-]/g, '_')

  redisClient.setex(`${topic_str}`, 3000, message_str, redis.print);
}

/**------------------------------------------------------------------------------------
 * Gps Information Client
 * ------------------------------------------------------------------------------------
 */
const gpsClient = mqtt.connect({
  host: process.env.MQTT_HOST,
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: false
});
const gpsTopic = "read/gps/#"
gpsClient.subscribe(gpsTopic)
gpsClient.on('message', async function (gpsTopic, message) {

    await saveRedis(gpsTopic, message)
})

/**------------------------------------------------------------------------------------
 * Mem Information Client
 * ------------------------------------------------------------------------------------
 */
const memInfoClient = mqtt.connect({
  host: process.env.MQTT_HOST,
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: false
});
const memInfoTopic = "read/mem_info/#"
memInfoClient.subscribe(memInfoTopic)
memInfoClient.on('message', async function (memInfoTopic, message) {

  // Map the individual json properties
  const json = JSON.parse(message)
  await saveRedis(`${memInfoTopic}:MemTotal`, json.MemTotal)
  await saveRedis(`${memInfoTopic}:MemFree`, json.MemFree)
  await saveRedis(`${memInfoTopic}:MemAvailable`, json.MemAvailable)

  // Map the json obj
  await saveRedis(memInfoTopic, message)
})



/**------------------------------------------------------------------------------------
 * Cpu Temp Client
 * ------------------------------------------------------------------------------------
 */
const cpuTempClient = mqtt.connect({
  host: process.env.MQTT_HOST,
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: false
});
const cpuTempTopic = "read/cpu_temp/#" 
cpuTempClient.subscribe(cpuTempTopic)
cpuTempClient.on('message', async function (cpuTempTopic, message) {

  const json = JSON.parse(message)
  // Map the individual json properties
  await saveRedis(cpuTempTopic, json.cpu_temp)
})