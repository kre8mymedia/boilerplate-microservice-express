#!/bin/bash

echo '/**
 * --------------------------------------------------------------------------------------
 * Resource Schema
 * --------------------------------------------------------------------------------------
 * @swagger
 * components:
 *   schemas:
 *     Resource:
 *       type: object
 *       properties:
 *         id:
 *           type: uuid
 *           description: The resource ID.
 *           example: 02092a15-dbcb-40b5-957c-ebd063bbb6e3
 */


/**
 * --------------------------------------------------------------------------------------
 * List resources
 * --------------------------------------------------------------------------------------
 * @swagger
 * /resources:
 *   get:
 *     tags:
 *       - Resources
 *     summary: List Resources
 *     description: List all resources that exist
 *     responses:
 *       200:
 *         description: A list of resources.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  resources:
 *                      items:
 *                        $ref: "#/components/schemas/Resource"
 */


/**
 * --------------------------------------------------------------------------------------
 * Create Resource
 * --------------------------------------------------------------------------------------
 * 
 * @swagger
 * /resources:
 *   post:
 *     tags:
 *       - Resources
 *     summary: Create Resource
 *     description: creates a new resource
 *     responses:
 *       201:
 *         description: Creates resource
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: "#/components/schemas/Resource"
 */ 
 
/**
 * --------------------------------------------------------------------------------------
 * Fetch Resource by ID
 * --------------------------------------------------------------------------------------
 * @swagger
 * /resources/{id}:
 *   get:
 *     tags:
 *       - Resources
 *     summary: Retreive Resource by ID
 *     description: retrieve resource by id
 *     responses:
 *       200:
 *         description: A single resource.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  resource:
 *                      type: object
 *                      $ref: "#/components/schemas/Resource"
 */


/**
 * --------------------------------------------------------------------------------------
 * Update Resource by ID
 * --------------------------------------------------------------------------------------
 * @swagger
 * /resources/{id}:
 *   put:
 *     tags:
 *       - Resources
 *     summary: Update Resource by ID
 *     description: retrieve resource by id
 *     responses:
 *       201:
 *         description: Update single resource.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  resource:
 *                      type: object
 *                      $ref: "#/components/schemas/Resource"
*/

/**
 * --------------------------------------------------------------------------------------
 * Delete Resource by ID
 * --------------------------------------------------------------------------------------
 * @swagger
 * /resources/{id}:
 *   delete:
 *     tags:
 *       - Resources
 *     summary: Delete Resource by ID
 *     description: Delete resource by id
 *     responses:
 *       201:
 *         description: Delete single resource.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  resource:
 *                      type: object
 *                      $ref: "#/components/schemas/Resource"
 */
 ' >> $1