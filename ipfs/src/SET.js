const { create } = require('ipfs-http-client');
const redis = require("redis")

const redisClient = redis.createClient({ host: 'redis', port: 6379});
redisClient.connect();

async function main(payload) {

    // connect to the default API address http://localhost:5001
    const ipfs = create('http://127.0.0.1:5001')
    const value = payload
    const file = await ipfs.add({
        path: "hello.json",
        content: JSON.stringify(value),
    });

    const id = "myRedisKeyForIpfsObj"
    const key = file.cid.toString()

    await redisClient.set(id, key);
    const result = await redisClient.get(id);

    const obj = {};
    obj[id] = result;
    console.log(obj)
}

main({ hair: "asdnew5nwe5w", age: 52, hasDogs: true });
