const { create } = require('ipfs-http-client');
const redis = require("redis")

const redisClient = redis.createClient({ host: 'redis', port: 6379});
redisClient.connect();

async function main() {
    
    const result = await redisClient.get("beta");

    // connect to the default API address http://localhost:5001
    const ipfs = create('http://127.0.0.1:5001')
    const stream = ipfs.cat(result)
    let data = ''

    for await (const chunk of stream) {
        // chunks of data are returned as a Buffer, convert it back to a string
        data += chunk.toString()
    }

    console.log(JSON.parse(data))
}

setInterval( async () => {
    await main();
}, 100)