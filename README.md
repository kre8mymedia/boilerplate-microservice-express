# Run to build services [Agent, Broker, Server] 
NOTE: You can also run the services individually on your host machine

### Table of Contents
- [Architecture Diagram](https://siasky.net/bAABnFcxvc1KygWdFao7jAMFUTRAn9dsrm0xbGh17WlNDA)
- [Swagger Express Docs](https://dev.to/kabartolo/how-to-document-an-express-api-with-swagger-ui-and-jsdoc-50do)
- [Prometheus Docs](https://prometheus.io/docs/introduction/overview/)
- [Grafana Docs](https://grafana.com/docs/)
- [Rabbit MQ Docs](https://www.rabbitmq.com/mqtt.html)
- [Node-RED Docs](https://nodered.org/docs/)


## API Server: Centralized authorizty for managing User, Groups, Permission
### Build
<pre>
cd server
docker-compose up --build
</pre>

### Run migrations
<pre>
<b>Run migration</b>
bash server/scripts/migrations/run.sh

<b>Seed Database (Optional)</b>
bash server/scripts/seeds/run.sh
</pre>
- Express Sequelize Connected to Redis and Mqtt available on http://localhost:5000
- PHPMyAdmin available on http://localhost:8081
    - Username: express
    - Password: test1234

### Connect to Redis CLI from within container
<pre>
<b>Exec into Redis container</b>
docker exec -it redis /bin/sh

<b>Log into Redis CLI</b>
redis-cli -h 127.0.0.1 -p 6379 -a test1234
</pre>
- Redis UI available on http://localhost:7843

## Broker: Link between Agents and API Server, Aggergate Data, Manages Alerts
<pre>
<b>Build Broker: RabbitMQ, Prometheus, Grafana</b>
cd broker
docker-compose up --build

<b>Exec into the RabbitMQ Container</b>
docker exec -it rabbitprod /bin/sh

<b>Create a Broker User (Default can be hassle)</b>
rabbitmqctl add_user test test
rabbitmqctl set_user_tags test administrator
rabbitmqctl set_permissions -p / test ".*" ".*" ".*"
</pre>

### MQTT Brokers
- Broker-DEV | available on mqtt://localhost:1883
    - Username: username
    - Password: password
- Broker-PROD | available on mqtt://localhost:1884
    - Username: username
    - Password: password

### Use ngrok to scrape the targets from localhost, Prometheus has trouble using docker to scrape host
<pre>
<b>Run this on whatever port to get generated url to service over internet</b>
ngrok http 8080
</pre>
### Graphical Interfaces
- RabbitMQ available on http://localhost:8080
    - Username: test
    - Password: test
- Prometheus available on http://localhost:9090
    - Username: admin
    - Password: test1234
- Grafana available on http://localhost:3000
    - Username: admin
    - Password: admin

## Agent: Sends and receives data every second to broker
<pre>
<b>Build Agent: Node-RED, Motion, IoT Scripts</b>
cd agent
docker-compose up --build
</pre>
- Node RED available on http://localhost:1880
    - Username: admin
    - Password: test1234
- Motion IP Camera available on http://localhost:8081