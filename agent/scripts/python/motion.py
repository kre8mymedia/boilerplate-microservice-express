import cv2 as cv
import time 


## Path to Capture
capture = cv.VideoCapture('http://localhost:8081')
## Obj detector
object_detector = cv.createBackgroundSubtractorMOG2(history=100, varThreshold=40)
## Start Loop
while True:
        ts = time.time()
        isTrue, frame = capture.read()
        ## Segment Vid Capture
        height, width, _ = frame.shape
#       print(height, width)
        ## Region of Interest [x1-x2, y1-y2]
        roi = frame[0: 600, 0: 600]

        ## Refine Mask
        mask = object_detector.apply(roi)
        _, mask = cv.threshold(mask, 254, 255, cv.THRESH_BINARY)
        contours, _ = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        detections = []

        ## Apply Features
        for cnt in contours:
                area = cv.contourArea(cnt)
                if area > 100:
#                       cv.drawContours(frame, [cnt], -1, (0, 255, 0), 2)
                        x, y, w, h = cv.boundingRect(cnt)
                        cv.rectangle(roi, (x, y), (x + w, y + h), (0, 255, 0), 3)
                        detections.append([x, y, w, h])

        print(ts, detections)
        ### Frame to show
        cv.imshow('roi', roi)
#       cv.imshow('Video', frame)
#       cv.imshow('Mask', mask)

        ## PRESS "d" to break process
        if cv.waitKey(20) & 0xFF==ord('d'):
                break
## CleanUP
capture.release()
cv.destroyAllWindows()
