const web3 = require('@solana/web3.js');
const {Keypair} = require("@solana/web3.js");
const nacl = require('tweetnacl');

async function main() {
    
    let payerSecretKey = Uint8Array.from([
        86, 184, 150, 238, 179, 209,   4,   6, 227, 164,  89,
       148,  25, 198,  49, 123,  84,  46, 219, 227, 222, 137,
       203, 139, 109, 189, 175, 211,  82, 155, 131, 165,  18,
       146, 205, 192, 190,  70,  99, 130, 102, 193, 193, 189,
       113,  16,   6,  82, 218, 131, 171,  44,  89, 172,  39,
        90, 213, 250, 114, 205,  53, 235,  36, 139
    ]);
    let payer = Keypair.fromSecretKey(payerSecretKey);
    console.log('Payer: ', payer)
    console.log('senderAddress: ', payer.publicKey.toBase58())

    // Pick network connection
    let connection = new web3.Connection(web3.clusterApiUrl('devnet'), 'confirmed');
    // Airdrop SOL for paying transactions
    // let airdropSignature = await connection.requestAirdrop(
    //     payer.publicKey,
    //     web3.LAMPORTS_PER_SOL,
    // );

    // await connection.confirmTransaction(airdropSignature);

    let receiverSecretKey = Uint8Array.from([
        245, 189,  80, 250, 194, 183, 146,  96,  43, 134,
        214,  35,  95, 163,  15,  82,  10,  96,  79, 223,
        112, 175, 241, 101, 198, 179, 141,  76, 101,  78,
         81, 164,  19,  53, 107, 228, 191, 142, 126, 165,
        124, 242, 187, 248, 148,  36, 114, 205, 229, 102,
        255, 162,  57, 244, 222,  72, 129, 213, 233, 185,
        213, 183, 159, 254
    ]);
    let toAccount = Keypair.fromSecretKey(receiverSecretKey);
    console.log('Receiver: ', toAccount)
    console.log('receiverAddress: ', toAccount.publicKey.toBase58())

    // Create Simple Transaction
    let transaction = new web3.Transaction();

    // Add an instruction to execute
    transaction.add(web3.SystemProgram.transfer({
        fromPubkey: payer.publicKey,
        toPubkey: toAccount.publicKey,
        lamports: 100000000,
    }));

    // Send and confirm transaction
    // Note: feePayer is by default the first signer, or payer, if the parameter is not set
    await web3.sendAndConfirmTransaction(connection, transaction, [payer])

    // Alternatively, manually construct the transaction
    let recentBlockhash = await connection.getRecentBlockhash();
    let manualTransaction = new web3.Transaction({
        recentBlockhash: recentBlockhash.blockhash,
        feePayer: payer.publicKey
    });
    manualTransaction.add(web3.SystemProgram.transfer({
        fromPubkey: payer.publicKey,
        toPubkey: toAccount.publicKey,
        lamports: 100000000,
    }));

    let transactionBuffer = manualTransaction.serializeMessage();
    let signature = nacl.sign.detached(transactionBuffer, payer.secretKey);

    manualTransaction.addSignature(payer.publicKey, signature);

    let isVerifiedSignature = manualTransaction.verifySignatures();
    console.log(`The signatures were verifed: ${isVerifiedSignature}`)

    // The signatures were verified: true

    let rawTransaction = manualTransaction.serialize();

    await web3.sendAndConfirmRawTransaction(connection, rawTransaction);
}

main()