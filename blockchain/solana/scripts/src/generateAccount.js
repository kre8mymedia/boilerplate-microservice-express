const web3 = require('@solana/web3.js');
const {Keypair} = require("@solana/web3.js");

// Create connection
function createConnection(url = "https://devnet.solana.com") {
  return new web3.Connection(url);
}

const connection = createConnection();

// Generate account
const bip39 = require('bip39');
const nacl = require('tweetnacl');

async function generateAccount(mnemonic) {
  const seed = await bip39.mnemonicToSeed(mnemonic);
  const keyPair = nacl.sign.keyPair.fromSeed(seed.slice(0, 32));
  return new web3.Account(keyPair.secretKey)
}

const mnemonic = bip39.generateMnemonic();
console.log("Your password:", mnemonic);

async function main() {
    const account = await generateAccount(mnemonic)
    let keypair = Keypair.fromSecretKey(account.secretKey);
    console.log("Your pubKey:", keypair.publicKey.toBase58());
}

main()

// Get balance
// function getBalance(publicKey) {
//   return connection.getBalance(publicKey);
// }

// const balance = getBalance(account.publicKey)

// // Send transaction
// async function sendTransaction(recipientPublicKey, recipientAmount) {
//   const transaction = new web3.Transaction().add(web3.SystemProgram.transfer({
//     fromPubkey: account.publicKey,
//     toPubkey: new web3.PublicKey(recipientPublicKey),
//     lamports: recipientAmount,
//   }));

//   const signature = await web3.sendAndConfirmTransaction(
//     connection,
//     transaction,
//     [account]
//   );
// }

// // Get history
// // https://solana-labs.github.io/solana-web3.js/class/src/connection.js~Connection.html#instance-method-getConfirmedSignaturesForAddress
// function getHistory(publicKey, options = { limit: 20 }) {
//   return connection.getConfirmedSignaturesForAddress2.getBalance(publicKey, options);
// }

// const history = getHistory(account.publicKey)

// // Airdrop request
// // lamports is 0.000000001 of SOL
// function requestAirdrop(publicKey, lamports = 1000000) {
//   return connection.requestAirdrop(publicKey, lamports)
// }
