require('dotenv').config()
const axios = require('axios').default;
const mqtt = require('mqtt')
const redis = require('redis');

const redisObj = {
    url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
}
const redisClient = redis.createClient(redisObj);

const mqttClient = mqtt.connect({
    host: process.env.MQTT_HOST,
    port: process.env.MQTT_PORT,
    username: process.env.MQTT_USERNAME,
    password: process.env.MQTT_PASSWORD,
    rejectUnauthorized: false
});


const API_URL = 'https://api.devnet.solana.com'

async function saveRedis(topic, message) {
    let topic_str = topic.toString()
    // let message_str = JSON.parse(message.toString())
    let message_str = message.toString()
    // split the topic str into array
    topic_str = topic_str
                      .replace(/[/]/g, ':')
                    //   .replace(/[-]/g, '_')
    redisClient.setex(topic_str, 3000, message_str, redis.print);
}

/**
 * Datasource 2
 */
const data1 = {
    jsonrpc:"2.0",
    id:1,
    method:"getBalance",
    params:["2FWB1LvNkzo5hWkp1hm3B67UqTSL7vCENRzawcZdqW5G"]
}
setInterval(async () => {
    try {
        const address = '2FWB1LvNkzo5hWkp1hm3B67UqTSL7vCENRzawcZdqW5G'
        const balance = await axios.post(API_URL, data1)
        const obj = {
            account: address,
            balance: balance.data.result.value
        }
        const topic = `read/solana/balances/${obj.account}`
        mqttClient.publish(`read/solana/balances/${obj.account}`, JSON.stringify(obj))
        await saveRedis(topic, obj.balance)
        console.log(obj)
    } catch(e) {
        console.log(e)
    }
}, 1000)

/**
 * Datasource 2
 */
const data2 = {
    jsonrpc:"2.0",
    id:1,
    method:"getBalance",
    params:["2HyzZfBDffxKCF8biMrVdVHipPjMr3xf53av3W6qPkr9"]
}
setInterval(async () => {
    try {
        const address = '2HyzZfBDffxKCF8biMrVdVHipPjMr3xf53av3W6qPkr9'
        const balance = await axios.post(API_URL, data2)
        const obj = {
            account: address,
            balance: balance.data.result.value
        }
        const topic = `read/solana/balances/${obj.account}`
        mqttClient.publish(`read/solana/balances/${obj.account}`, JSON.stringify(obj))
        await saveRedis(topic, obj.balance)
        console.log(obj)
    } catch(e) {
        console.log(e)
    }
}, 1000)